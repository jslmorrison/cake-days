<?php

namespace spec\App\Service;

use App\Service\OfficeManager;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class OfficeManagerSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(OfficeManager::class);
    }

    public function it_should_know_when_the_office_is_open_or_closed()
    {
        $this->isOfficeOpenOnDate(new \DateTime())->shouldBeBoolean();
        $this->isOfficeOpenOnDate(new \DateTime('first saturday of this month'))->shouldReturn(false);
        $this->isOfficeOpenOnDate(new \DateTime('first friday of this month'))->shouldReturn(true);
        $this->isOfficeOpenOnDate(new \DateTime('first friday of this month'))->shouldReturn(true);
    }

    public function it_should_know_if_date_is_on_weekend()
    {
        $this->isDateOnWeekend(new \DateTime())->shouldBeBoolean();
        $this->isDateOnWeekend(new \DateTime('next saturday'))->shouldReturn(true);
        $this->isDateOnWeekend(new \DateTime('next sunday'))->shouldReturn(true);
        $this->isDateOnWeekend(new \DateTime('next monday'))->shouldReturn(false);
    }

    public function it_should_know_if_date_is_on_a_holiday()
    {
        $this->isDateOnAHoliday(new \DateTime())->shouldBeBoolean();
        $this->isDateOnAHoliday(new \DateTime('25th December'))->shouldReturn(true);
        $this->isDateOnAHoliday(new \DateTime('2nd January'))->shouldReturn(false);
    }

    // birthday on a thursday, return to work on friday - no holidays
    public function it_should_know_when_employee_returns_to_work_after_birthday_scenario1()
    {
        $birthday = new \DateTime('2018-11-15');
        $this->returnToWorkDateAfterBirthday($birthday)
            ->shouldBeLike($birthday->add(new \DateInterval('P1D')));
    }

    // birthday on a friday, return to work on monday - no holidays
    public function it_should_know_when_employee_returns_to_work_after_birthday_scenario2()
    {
        $birthday = new \DateTime('2018-11-16');
        $this->returnToWorkDateAfterBirthday($birthday)
            ->shouldBeLike($birthday->add(new \DateInterval('P3D')));
    }

    // birthday on a saturday, return to work on tuesday - no holidays
    public function it_should_know_when_employee_returns_to_work_after_birthday_scenario3()
    {
        $birthday = new \DateTime('2018-11-17');
        $this->returnToWorkDateAfterBirthday($birthday)
            ->shouldBeLike($birthday->add(new \DateInterval('P3D')));
    }

    // birthday on a sunday, return to work on tuesday - no holidays
    public function it_should_know_when_employee_returns_to_work_after_birthday_scenario4()
    {
        $birthday = new \DateTime('2018-11-18');
        $this->returnToWorkDateAfterBirthday($birthday)
            ->shouldBeLike($birthday->add(new \DateInterval('P2D')));
    }

    public function it_should_know_when_employee_returns_to_work_after_birthday_scenario5()
    {
        $birthday = new \DateTime('2018-12-25');
        $this->returnToWorkDateAfterBirthday($birthday)
            ->shouldBeLike($birthday->add(new \DateInterval('P3D')));
    }

    public function it_should_know_holiday_dates()
    {
        $this->holidays()->shouldBeArray();
    }
}
