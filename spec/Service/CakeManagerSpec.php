<?php

namespace spec\App\Service;

use App\Service\CakeManager;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use App\Service\OfficeManager;

class CakeManagerSpec extends ObjectBehavior
{
    const DATA =  [
        [
            'date' => '2018-01-02',
        ],
        [
            'date' => '2018-01-03',
        ],
        [
            'date' => '2018-01-09',
        ],
        [
            'date' => '2018-01-09',
        ],
        [
            'date' => '2018-06-28',
        ],
        [
            'date' => '2018-07-02',
        ],
        [
            'date' => '2018-07-03',
        ],
        [
            'date' => '2018-07-04',
        ],
        [
            'date' => '2018-07-10',
        ],
        [
            'date' => '2018-07-11',
        ],
        [
            'date' => '2018-07-12',
        ]
    ];

    public function it_is_initializable()
    {
        $this->shouldHaveType(CakeManager::class);
    }

    public function let(\App\Service\OfficeManager $officeManager)
    {
        $this->beConstructedWith($officeManager);
    }

    public function it_should_find_any_coincidental_cake_days()
    {
        $data = [
            [
            'date' => '2018-01-02'
            ],
            [
                'date' => '2018-01-03'
            ],
            [
                'date' => '2018-01-09'
            ],
            [
                'date' => '2018-01-09'
            ]
        ];
        $coincedentals = [
            '2018-01-09'
        ];
        $this->findCoincidentalCakeDays($data)->shouldReturn($coincedentals);
    }

    public function it_should_find_subsequent_cake_days()
    {
        $data = [
            [
                'date' => '2018-07-02'
            ],
            [
                'date' => '2018-07-03'
            ],
            [
                'date' => '2018-07-05'
            ],
            [
                'date' => '2018-07-09'
            ],
            [
                'date' => '2018-07-11'
            ],
            [
                'date' => '2018-07-12'
            ]
        ];
        $subsequents = [
            [
                'firstDay' => new \DateTime('2018-07-02'),
                'secondDay' => new \DateTime('2018-07-03')
            ],
            [
                'firstDay' => new \DateTime('2018-07-11'),
                'secondDay' => new \DateTime('2018-07-12')
            ]
        ];
        $this->findSubsequentCakedays($data)->shouldBeLike($subsequents);
    }

    public function it_should_know_when_cake_free_days_are()
    {
    }

    public function it_should_throw_exeception_if_more_than_one_cake_per_day_found()
    {
    }

    public function it_should_process_employee_data(\App\Service\OfficeManager $officeManager)
    {
        $employeeData = [
            [
                'person' => 'Elvis Presley',
                'dob' => '1935-01-08'
            ],
            [
                'person' => 'David Bowie',
                'dob' => '1947-01-08'
            ],
            [
                'person' => 'Sam Sam',
                'dob' => '1980-07-28'
            ],
            [
                'person' => 'Kate Kate',
                'dob' => '1980-07-29'
            ]
        ];
        $officeManager->returnToWorkDateAfterBirthday(Argument::type('DateTime'))
            ->shouldBeCalled()
            ->willReturn(new \DateTime());
        $this->process($employeeData)->shouldBeArray();
    }

    public function it_should_return_cake_days_array(OfficeManager $officeManager)
    {
        $employeeData = [
            [
                'person' => 'Elvis Presley',
                'dob' => '1935-01-08'
            ],
            [
                'person' => 'David Bowie',
                'dob' => '1947-01-08'
            ],
            [
                'person' => 'Sam Sam',
                'dob' => '1980-07-28'
            ],
            [
                'person' => 'Kate Kate',
                'dob' => '1980-07-29'
            ]
        ];
        $officeManager->returnToWorkDateAfterBirthday(Argument::type('DateTime'))
            ->shouldBeCalled()
            ->willReturn(new \DateTime('2018-01-09'));
        $this->cakeDays($employeeData)->shouldBeArray();
    }
}
