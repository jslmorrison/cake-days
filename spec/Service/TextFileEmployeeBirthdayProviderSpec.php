<?php

namespace spec\App\Service;

use App\Service\TextFileEmployeeBirthdayProvider;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class TextFileEmployeeBirthdayProviderSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(TextFileEmployeeBirthdayProvider::class);
    }

    public function it_should_implement_interface()
    {
        $this->shouldImplement(\App\Service\EmployeeBirthdayProvider::class);
    }

    public function it_should_get_employee_birthday_details_from_text_file_as_array()
    {
        $this->details()->shouldBeArray();
    }
}
