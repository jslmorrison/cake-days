<?php

namespace spec\App\Service;

use App\Service\CsvReporter;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CsvReporterSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(CsvReporter::class);
    }
}
