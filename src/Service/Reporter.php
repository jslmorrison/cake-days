<?php

namespace App\Service;

interface Reporter
{
    public function report(array $sourceData);
}
