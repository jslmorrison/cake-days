<?php

namespace App\Service;

interface EmployeeBirthdayProvider
{
    public function details():array;
}
