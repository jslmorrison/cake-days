<?php

namespace App\Service;

use App\Service\OfficeManager;

class CakeManager
{
    private $officeManager;

    public function __construct(OfficeManager $officeManager)
    {
        $this->officeManager = $officeManager;
    }

    public function cakeDays(array $employeeData):array
    {
        $cakeDays = $this->process($employeeData);
        return $cakeDays;
    }

    public function findCoincidentalCakeDays(array $rawData): array
    {
        $tmp = [];
        $coincidentals = [];
        foreach ($rawData as $data) {
            if (!in_array($data['date'], $tmp)) {
                array_push($tmp, $data['date']);
            } elseif (!in_array($data['date'], $coincidentals)) {
                array_push($coincidentals, $data['date']);
            }
        }
        return $coincidentals;
    }

    public function findSubsequentCakedays(array $rawData):array
    {
        $tmp = [];
        $subsequents = [];
        $i = 0;
        foreach ($rawData as $data) {
            $tmp[] = new \DateTime($data['date']);
            if (array_key_exists($i - 1, $tmp)) {
                $interval = $tmp[$i-1]->diff(new \DateTime($data['date']));
                if ($interval->invert === 0 && $interval->d === 1) {
                    $subsequent = [
                        'firstDay' => $tmp[$i-1],
                        'secondDay' => $tmp[$i]
                    ];
                    $subsequents[] = $subsequent;
                }
            }
            $i++;
        }
        return $subsequents;
    }
    
    public function process(array $employeeData):array
    {
        $tmpCakeDays = [];
        $cakeDays = [];

        $iterable = new \ArrayObject($employeeData);
        $iterator = $iterable->getIterator();
        while ($iterator->valid()) {
            $birthday = explode('-', $iterator->current()['dob']);
            $tmpCakeDays[$iterator->key()] = $iterator->current();
            $tmpCakeDays[$iterator->key()]['birthday'] = date('Y').'-'.$birthday[1].'-'.$birthday[2];
            $tmpCakeDays[$iterator->key()]['date'] = $this->officeManager
                ->returnToWorkDateAfterBirthday(new \DateTime($iterator->current()['dob']));
            $iterator->next();
        }
        // dump($tmpCakeDays);
        $coincidentals = $this->findCoincidentalCakeDays($tmpCakeDays);
        // dump('coincidentals', $coincidentals);
        
        $iterable = new \ArrayObject($tmpCakeDays);
        $iterator = $iterable->getIterator();
        while ($iterator->valid()) {
            // dump(in_array($iterator->current()['date'], $coincidentals));
            if (!in_array($iterator->current()['date'], $coincidentals)) {
                $cakeDays[$iterator->key()] = [
                    'date' => $iterator->current()['date'],
                    'numSmallCakes' => 1,
                    'numLargeCakes' => 0,
                    'peopleGettingCake' => [$iterator->current()['person']]
                ];
            }
            if (in_array($iterator->current()['date'], $coincidentals)) {
                // dump($tmpCakeDays[$iterator->key()]);
                $cakeDays[$iterator->key()] = [
                    'date' => $iterator->current()['date'],
                    'numSmallCakes' => 0,
                    'numLargeCakes' => 1,
                    'peopleGettingCake' => [
                        $tmpCakeDays[$iterator->key()]['person'],
                        $tmpCakeDays[$iterator->key() + 1]['person']
                    ]
                ];
                $iterator->next();
            }
            $iterator->next();
        }
        // dump($cakeDays);
        return $cakeDays;
    }
}
