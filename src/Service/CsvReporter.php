<?php

namespace App\Service;

use League\Csv\Writer;

class CsvReporter implements Reporter
{
    public function report(array $cakeDays)
    {
        $writer = Writer::createFromPath(__DIR__.'/../../data/cakeDays.csv', 'w+');
        $headers = ['Date', 'Number of small cakes', 'Number of large cakes', 'Names of people getting cake'];
        $writer->insertOne($headers);

        $recipients = '';
        foreach ($cakeDays as $row) {
            $row['date'] = $row['date']->format('Y-m-d');
            $row['peopleGettingCake'] = implode(', ', array_values($row['peopleGettingCake']));
            // dump($row);
            $writer->insertOne($row);
        }
    }
}
