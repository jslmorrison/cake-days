<?php

namespace App\Service;

use App\Service\EmployeeBirthdayProvider;

class TextFileEmployeeBirthdayProvider implements EmployeeBirthdayProvider
{
    const SOURCE_FILE = 'employeeBirthdays.txt';

    public function details():array
    {
        $details = [];
        $fileObject = new \SplFileObject(__DIR__.'/../../data/'.self::SOURCE_FILE);
        $fileObject->setFlags(\SplFileObject::SKIP_EMPTY);
        while ($fileObject->valid()) {
            $exploded = explode(',', $fileObject->current());
            if (count($exploded) === 2) {
                $details[] = [
                    'person' => trim($exploded[0]),
                    'dob' => trim($exploded[1])
                ];
            }
            $fileObject->next();
        }
        // todo - fixme - sorting breaks things currently
        // usort($details, function ($a, $b) {
        //     return ($a['dob'] < $b['dob']) ? -1 : 1;
        // });
        // dump($details);
        return $details;
    }
}
