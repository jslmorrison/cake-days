<?php

namespace App\Service;

class OfficeManager
{
    const XMAS_DAY = '25th December';
    const BOXING_DAY = '26th December';
    const NEW_YEAR_DAY = '1st January';

    public function isOfficeOpenOnDate(\DateTime $dateTime):bool
    {
        if ($this->isDateOnWeekend($dateTime) === true ||
            $this->isDateOnAHoliday($dateTime) === true
        ) {
            return false;
        }
        return true;
    }

    public function isDateOnWeekend(\DateTime $dateTime):bool
    {
        $dayOfWeek = $dateTime->format('N');
        if ($dayOfWeek >= 6) {
            return true;
        }
        return false;
    }

    public function isDateOnAHoliday(\DateTime $dateTime):bool
    {
        return in_array($dateTime, $this->holidays());
    }

    public function returnToWorkDateAfterBirthday(\DateTime $birthday):\DateTime
    {
        $birthdayThisYear = new \DateTime(date('Y').'-'.$birthday->format('m').'-'.$birthday->format('d'));
        $returnToWork = clone $birthdayThisYear;
        $returnToWork->add(new \DateInterval('P1D'));
        while ($this->isOfficeOpenOnDate($returnToWork) === false) {
            $returnToWork->add(new \DateInterval('P1D'));
        }
        if ($this->isOfficeOpenOnDate($birthdayThisYear) === false) {
            $returnToWork->add(new \DateInterval('P1D'));
        }
        
        return $returnToWork;
    }

    public function holidays():array
    {
        return [
            new \DateTime(self::XMAS_DAY),
            new \DateTime(self::BOXING_DAY),
            new \DateTime(self::NEW_YEAR_DAY)
        ];
    }
}
