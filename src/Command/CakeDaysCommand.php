<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\EmployeeBirthdayProvider;
use App\Service\CakeManager;
use App\Service\CsvReporter;

class CakeDaysCommand extends Command
{
    private $provider;
    private $cakeManager;
    private $csvReporter;

    public function __construct(
        EmployeeBirthdayProvider $provider,
        CakeManager $cakeManager,
        CsvReporter $csvReporter
    ) {
        $this->provider = $provider;
        $this->cakeManager = $cakeManager;
        $this->csvReporter = $csvReporter;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:cakedays:run')
            ->setDescription('Provides info of when the cake days are for employee birthdays');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>starting....</info>');
        $data = $this->provider
            ->details();
        if (empty($data)) {
            $output->writeln('<error>There is no data to process - so no cakes for you :(</error>');
        }
        if (!empty($data)) {
            try {
                $cakeDays = $this->cakeManager
                    ->process($data);
                // dump($cakeDays);
                $this->csvReporter->report($cakeDays);
                $output->writeln('<comment>csv file has been generated from data - in same directory as employee birthday details text file :)</comment>');
            } catch (\Throwable $e) {
                $output->writeln('<error>'.$e->getMessage().'</error>');
            }
        }
        $output->writeln('<info>...finished</info>');
    }
}
