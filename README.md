# CakeDays
A cli app to generate csv file with data about cake days from data regarding employees and thier birthdays.
## Running
Assuming you have Docker and docker-compose installed on your system:

`docker-compose up -d`
to start the container

then

`docker-compose exec cli bin/console app:cakedays:run`

Or without docker 

`php bin/console app:cakedays:run`
 - requires php v7.2

## Tests
`docker-compose exec cli vendor/bin/phpspec run`

## Known issues
`src/Service/TextFileEmployeeBirthdayProvider.php`

Breaks when sorting the employee data by date of birth - oops.
A couple of incomplete specs